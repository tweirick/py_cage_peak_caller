### py_cage_peak_caller 

This pipeline calls and annotates CAGE peaks. 

Usage example:
```bash
snakemake --snakefile cage_annotator.snakemake --configfile config.yaml
```

Contents:
- cage_annotator.snakemake - pipeline
- config.yaml - example config file

The program will create a directory containing the following: 
- CAGE peaks. 
- ...


